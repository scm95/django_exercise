from django.db import models
from django.core.validators import RegexValidator

# Create your models here.
class BaseCRUDModel(models.Model):
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		abstract = True

class MonitoredInterface(BaseCRUDModel):
	external_id = models.CharField(max_length=120, null=False, blank=False)
	mac_address = models.CharField(max_length=12, null=False, blank=False, validators=[
							RegexValidator(regex=r"^[A-F0-9:]{12,17}$",
											message="Mac Address is not valid",
											code="invalid_mac_address")])
	ipv4_address = models.GenericIPAddressField(protocol="IPv4", null=False, blank=False)
	interface_name = models.CharField(max_length=50, null=False, blank=False)

	class Meta:
		unique_together = [['mac_address', 'interface_name'],]

	def __str__(self):
		return f'{self.interface_name} on {self.mac_address}'

