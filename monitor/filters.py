from .models import MonitoredInterface
import django_filters

class MonitoredInterfaceFilter(django_filters.FilterSet):
    class Meta:
        model = MonitoredInterface
        fields = ['mac_address', 'interface_name' ]