from django.contrib import admin

from .models import (MonitoredInterface)

# Register your models here.
@admin.register(MonitoredInterface)
class MonitoredInterfaceAdmin(admin.ModelAdmin):
	list_filter = ['interface_name']
	list_display = ['external_id','mac_address','ipv4_address','interface_name']
