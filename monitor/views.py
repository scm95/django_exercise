from django.shortcuts import render, redirect
from django.views import generic
from django.db.models import Q
import re

import requests

from .forms import (
    AddResourceForm,
)
from .models import MonitoredInterface

from .filters import MonitoredInterfaceFilter

def search(request):
    interface_list = MonitoredInterface.objects.all()
    interface_filter = MonitoredInterfaceFilter(request.GET, queryset=interface_list)
    return render(request, 'monitor/list.html', {'filter': interface_filter})

def add_resource(request):
    template_name = 'monitor/add_resource.html'
    regex_pattern = re.compile("^[A-F0-9:]{12,17}$")
    if request.method == 'GET':
        form = AddResourceForm(request.GET or None)
        messages = []
    elif request.method == 'POST':
        form = AddResourceForm(request.POST)
        monitoredInterface = MonitoredInterface()
        messages = []
        if form.is_valid():
            url = form.cleaned_data['url']
            try:
                response = requests.get(url)
                data = response.json()
                for entry in data:
                    if 'interfaces' in entry.keys():
                        mac_address = str(entry['mac_address'])
                        if ":" in mac_address:
                            mac_address = mac_address.replace(":","")
                        if regex_pattern.match(mac_address):
                            for item in entry['interfaces']:
                                obj, created = MonitoredInterface.objects.update_or_create(
                                    interface_name=item['name'], mac_address=mac_address,
                                    defaults = {'external_id':item['id'], 'ipv4_address':item['ipv4']} 
                                )
                    else:
                        mac_address = str(entry['mac'])
                        if ":" in mac_address:
                            mac_address = mac_address.replace(":","")
                        if regex_pattern.match(mac_address):
                            obj, created = MonitoredInterface.objects.update_or_create(
                                    interface_name=entry['interface'], mac_address=mac_address,
                                    defaults = {'external_id':entry['guid'], 'ipv4_address':entry['address']} 
                                )
                invalid_entries = MonitoredInterface.objects.filter(
                    Q(interface_name__exact='') | Q(mac_address__exact='')| Q(external_id__exact='')
                    | Q(ipv4_address__exact='')) 
                invalid_entries.delete()
                return redirect('monitor:monitor_list')
            except requests.exceptions.RequestException as e:
                messages = ["The URL Resource is not reachable. Please ensure that it is correct"]
    return render(request, template_name, {
        'form': form,
        'messages': messages,
    })

class MonitoredInterfaceListView(generic.ListView):
    model = MonitoredInterface
    context_object_name = 'interfaces'
    template_name = 'monitor/list.html'

    def get_context_data(self, **kwargs):
        context = super(MonitoredInterfaceListView, self).get_context_data(**kwargs)
        context['count'] = self.get_queryset().count()
        return context
