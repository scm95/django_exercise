from django import forms

from .models import (MonitoredInterface)


class AddResourceForm(forms.Form):
    url = forms.URLField(
        label='URL of Resource',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Please enter the URL here'
        })
    )

