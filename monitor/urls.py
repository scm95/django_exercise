from django.urls import path
from django_filters.views import FilterView
from .filters import MonitoredInterfaceFilter


from .views import (
    MonitoredInterfaceListView,
    add_resource,
    search
)

app_name = 'monitor'

urlpatterns = [

    path('', MonitoredInterfaceListView.as_view(), name='monitor_list'),
    path('add_resource/', add_resource, name='add_resource'),
    path('search/', FilterView.as_view(filterset_class=MonitoredInterfaceFilter,
        template_name='monitor/search.html'), name='search'),

]
