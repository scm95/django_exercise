# Generated by Django 3.0.5 on 2020-05-07 22:26

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('monitor', '0004_auto_20200507_1702'),
    ]

    operations = [
        migrations.AlterField(
            model_name='monitoredinterface',
            name='mac_address',
            field=models.CharField(max_length=12, validators=[django.core.validators.RegexValidator(code='invalid_mac_address', message='Mac Address is not valid', regex='^[A-F0-9:]{12,17}$')]),
        ),
    ]
